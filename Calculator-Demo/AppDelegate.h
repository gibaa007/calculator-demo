//
//  AppDelegate.h
//  Calculator-Demo
//
//  Created by LordGanesh on 26/09/15.
//  Copyright (c) 2015 LordGanesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

