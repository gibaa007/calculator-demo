//
//  ViewController.m
//  Calculator-Demo
//
//  Created by LordGanesh on 26/09/15.
//  Copyright (c) 2015 LordGanesh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic) BOOL completedOperation;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction) clearDisplay {
    display.text = @"";
}

- (IBAction) button1 {
    if([display.text isEqualToString:@"0"]){
        display.text =@"";
    }
    if(self.completedOperation){
        display.text =@"";
        self.completedOperation =NO;
    }
    display.text=[NSString stringWithFormat:@"%@1",display.text];
}
- (IBAction) button2 {
    if([display.text isEqualToString:@"0"]){
        display.text =@"";
    }
    if(self.completedOperation){
        display.text =@"";
        self.completedOperation =NO;
    }
    display.text=[NSString stringWithFormat:@"%@2",display.text];
}
- (IBAction) button3 {
    if([display.text isEqualToString:@"0"]){
        display.text =@"";
    }
    if(self.completedOperation){
        display.text =@"";
        self.completedOperation =NO;
    }
    display.text=[NSString stringWithFormat:@"%@3",display.text];
}

- (IBAction) button4 {
    if([display.text isEqualToString:@"0"]){
        display.text =@"";
    }
    if(self.completedOperation){
        display.text =@"";
        self.completedOperation =NO;
    }
    display.text=[NSString stringWithFormat:@"%@4",display.text];
}

- (IBAction) button5 {
    if([display.text isEqualToString:@"0"]){
        display.text =@"";
    }
    if(self.completedOperation){
        display.text =@"";
        self.completedOperation =NO;
    }
    display.text=[NSString stringWithFormat:@"%@5",display.text];
}

- (IBAction) button6 {
    if([display.text isEqualToString:@"0"]){
        display.text =@"";
    }
    if(self.completedOperation){
        display.text =@"";
        self.completedOperation =NO;
    }
    display.text=[NSString stringWithFormat:@"%@6",display.text];
}

- (IBAction) button7 {
    if([display.text isEqualToString:@"0"]){
        display.text =@"";
    }
    if(self.completedOperation){
        display.text =@"";
        self.completedOperation =NO;
    }
    display.text=[NSString stringWithFormat:@"%@7",display.text];
}

- (IBAction) button8 {
    if([display.text isEqualToString:@"0"]){
        display.text =@"";
    }
    if(self.completedOperation){
        display.text =@"";
        self.completedOperation =NO;
    }
    display.text=[NSString stringWithFormat:@"%@8",display.text];
}

- (IBAction) button9 {
    if([display.text isEqualToString:@"0"]){
        display.text =@"";
    }
    if(self.completedOperation){
        display.text =@"";
        self.completedOperation =NO;
    }
    display.text=[NSString stringWithFormat:@"%@9",display.text];
}

- (IBAction) button0 {
    if([display.text isEqualToString:@"0"]){
        display.text =@"";
    }
    if(self.completedOperation){
        display.text =@"";
        self.completedOperation =NO;
    }
    display.text=[NSString stringWithFormat:@"%@0",display.text];
}

- (IBAction) plusbutton {
    operation = Plus;
    storage = display.text;
    display.text=@"";
}

- (IBAction) minusbutton {
    operation = Minus;
    storage = display.text;
    display.text=@"";
}

- (IBAction) multiplybutton {
    operation = Multiply;
    storage = display.text;
    display.text=@"";
}

- (IBAction) dividebutton {
    operation = Divide;
    storage = display.text;
    display.text=@"";
}

- (IBAction)percentagebutton:(id)sender {
    NSString *val = display.text;
    self.completedOperation = YES;
    switch(operation) {
        case Multiply :
            display.text= [NSString stringWithFormat:@"%f",([val doubleValue]/100)*[storage doubleValue]];
            break;
            default:
            display.text = [NSString stringWithFormat:@"%f",([val doubleValue]/100)];
            break;
    }
}

- (IBAction)pointButton:(id)sender {
    if ([display.text isEqualToString:@""]) {
        display.text =@"0";
    }
    display.text=[NSString stringWithFormat:@"%@.",display.text];
}

- (IBAction) equalsbutton {
    NSString *val = display.text;
    self.completedOperation = YES;
    switch(operation) {
        case Plus :
            display.text= [NSString stringWithFormat:@"%f",[val doubleValue]+[storage doubleValue]];
            break;
        case Minus:
            display.text= [NSString stringWithFormat:@"%f",[storage doubleValue]-[val doubleValue]];
            break;
        case Divide:
            display.text= [NSString stringWithFormat:@"%f",[storage doubleValue]/[val doubleValue]];
            break;
        case Multiply:
            display.text= [NSString stringWithFormat:@"%f",[val doubleValue]*[storage doubleValue]];
            break;
    }
}

@end
